import React from 'react';
import { useParams } from "react-router-dom";

function CompanyDetails({location}) {
    return (
        <div className="companyDetails">
            <h2>{location.state.name}</h2>
            <p>{location.state.address}</p>
        </div>
    )
}

export default CompanyDetails;
