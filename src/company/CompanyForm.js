import React, {useState, useEffect} from 'react';
import './CompanyForm.css';
import TextField from '../components/text-field';
import Button from '../components/button';
import { useForm } from '../hooks/useForm';
import {validatCompanyData} from '../validation';
import apiService from '../services/apiService';
import AlertMessage from '../components/Alert';

const defaultValues = {
    company: {
        name: '',
        address: '',
        // tin: ''
    },person: {
        firstName: '',
        lastName: '',
    }, user: {
        username: '',
        // email: '',
        password: '',
        confirmPassword: ''
    },
}

function CompanyForm() {

    const {
        values, 
        handleOnChange, 
        handleOnSubmit, 
        errors, 
        setValues,
        setErrors} = useForm(defaultValues, submit, validatCompanyData);

    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [message, setMessage] = useState('');

    function submit(){
        setIsLoading(true);
        apiService.save("/company/register", values)
            .then((response) => {
                if(response.data.success) {
                    setIsLoading(false)
                    setIsSuccess(true);
                    setMessage("Record save!")
                    setValues({...defaultValues})
                }
            }).catch((e) => {
                setIsLoading(false)
                setValues({...defaultValues})
                console.log("Error", e);
            })
    }

    useEffect(() => {
        const timer = setTimeout(() => {
            setIsSuccess(false);
        }, 2000);
        return () => clearTimeout(timer);
      }, [isSuccess]);


    return (
        <div className="companyForm">
            {isSuccess ? <AlertMessage message={message}/> : "" }
             <form 
                onSubmit={handleOnSubmit}
                noValidate>
                <h2>Add Company</h2>
                <div className="company_form__textField">
                    <div className="company__form__col">
                        <div className="company__form__info">
                            <h3>Company Information</h3>
                            <TextField 
                                type="text"
                                label="Company Name" 
                                name="name"
                                value={values.company.name}
                                onChange={handleOnChange("company")}
                                errors={errors.name} />
                                
                            <TextField 
                                type="text"
                                label="Address"
                                name="address"
                                value={values.company.address}
                                onChange={handleOnChange("company")}
                                errors={errors.address}/>
                                
                            {/* <TextField 
                                type="number"
                                label="TIN no."
                                name="tin"
                                value={values.company.tin}
                                onChange={handleOnChange("company")}
                                errors={errors.tin}/> */}

                        </div>
                        <div className="company__form__admin">
                            <h3>Admin Information</h3>
                            <TextField 
                                type="text"
                                label="First name"
                                name="firstName"
                                value={values.person.firstName}
                                onChange={handleOnChange("person")}
                                errors={errors.firstName} />

                            <TextField 
                                type="text"
                                label="Last name"
                                name="lastName"
                                value={values.person.lastName}
                                onChange={handleOnChange("person")}
                                errors={errors.lastName}/>
                        </div>
                    </div>

                    <div className="company__form__col">
                        <h3>Log-in Information</h3>
                        <TextField 
                            type="text"
                            label="Username"
                            name="username"
                            value={values.user.username}
                            onChange={handleOnChange("user")}
                            errors={errors.username}/>
                        
                        {/* <TextField 
                            type="text"
                            label="Email address"
                            name="email"
                            value={values.user.email}
                            onChange={handleOnChange("user")}
                            errors={errors.email}/> */}
                        
                        <TextField 
                            type="password"
                            label="Password"
                            name="password"
                            value={values.user.password}
                            onChange={handleOnChange("user")}
                            errors={errors.password}/>

                        <TextField 
                            type="password"
                            label="Confirm password"
                            name="confirmPassword"
                            value={values.user.confirmPassword}
                            onChange={handleOnChange("user")}
                            errors={errors.confirmPassword}/>
                    </div>
                </div>
                <div className="company__form__col">
                    <Button color="primary" size="size" disabled={isLoading}>{isLoading ? "Loading..." : "Submit"}</Button>
                </div>
            </form>
        </div>
    )
}

export default CompanyForm
