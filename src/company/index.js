import React, {useState} from 'react';
import './Company.css';
import {tableConstant} from './tableConstant';
import Table from '../components/table';
import useGetData from '../hooks/useGetData';
import {Redirect, useHistory, Link, Switch, Route} from 'react-router-dom';
import CompanyDetails from './CompanyDetails';
import CompanyForm from './CompanyForm';

function Company() {
    
    const {response, isLoading} = useGetData("company");

    return (
        <div className="company">
            <Switch>
                <Route exact path="/company">
                    <h2>Your Company</h2>
                    <Table cols={tableConstant(Link)} data={response.data} />
                </Route>
                <Route path="/company/new" component={CompanyForm} />
                <Route path="/company/:id" component={CompanyDetails} />
            </Switch>
        </div>
    )
}

export default Company;
