import React from 'react'

export const tableConstant = (Link) => {
    return [
        {
            title: 'Name',
            render: rowData => {
                return <span>{rowData.name}</span>
            }
        },
        {
            title: 'Address',
            render: rowData => {
                return <span>{rowData.address}</span>
            }
        },
        {
            title: '',
            render: rowData => {
                return <Link to={{
                    pathname:"/company/" + rowData.id,
                    state: rowData
                }}>View</Link>
            }
        },
    ]
}
