import React, {useEffect, useState} from 'react'
import Alert from '@material-ui/lab/Alert';
import './index.css';

function AlertMessage({message}) {
    
    return (
        <Alert className="alert" severity="success">{message}</Alert>
    )
}

export default AlertMessage;
