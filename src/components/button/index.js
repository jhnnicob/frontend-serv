import React from 'react';
import './Button.css';

export default function Button({children, 
    color, 
    size, 
    fullWidth, 
    onClick,
    disabled}) {
    return (
        <button 
            disabled={disabled}
            onClick={onClick}
            className={`btn ${disabled ? "disabled" : color}`}>
            {children}
        </button>
    )
}