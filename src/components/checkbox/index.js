import React from 'react';
import './Checkbox.css';
export default function Checkbox({label, name, onChange, checked}) {
    return (
        <div className="checkbox">
            <input 
                type="checkbox" 
                name={name}
                checked={checked}
                onChange={onChange}/>
            <label>{label}</label>
        </div>
    )
}
