import React, {useState, useRef} from 'react';
import './index.css';

function Select({
    value, 
    options, 
    name, 
    label,
    onChange,
    errors,}) {

    const [focused, setFocused] = useState(false);
    const ref = useRef(null);

    return(
        <div
            className={`form-field 
                ${focused && focused ? 'is-focused' : ''} 
                ${value && value.length > 0 ? 'has-value' : ''}`}>
            <div className="control">
            <label onClick={() => ref.current.focus()}>{label}</label>
                <select  
                    ref={ref}  
                    name={name}
                    value={value}
                    onChange={onChange}
                    onFocus={() => setFocused(true)}
                    onBlur={(e) => {
                        setFocused(false);
                    }}>
                    {options && options.map((option, index) => (
                        <option key={index} value={option.name}>
                            {option.name}</option>
                    ))}
                </select>
            </div>
            {
                errors ? <span className="errorMessage">{errors}</span> : ""
            }
        </div>
    )
}

export default Select;
