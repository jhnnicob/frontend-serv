import React,{useState, useRef} from 'react';
import "./TextField.css";

export default function TextField({
    type,
    name, 
    label,
    value,
    onChange,
    errors,
}) {
  
    const [focused, setFocused] = useState(false);
    const ref = useRef(null);

    return(
        <div
            className={`form-field 
                ${focused && focused ? 'is-focused' : ''} 
                ${value && value.length > 0 ? 'has-value' : ''}`}>
            <div className="control">
            <label onClick={() => ref.current.focus()}>{label}</label>
                <input 
                    ref={ref}
                    type={type}  
                    name={name}
                    value={value}
                    onChange={onChange}
                    onFocus={() => setFocused(true)}
                    onBlur={(e) => {
                        setFocused(false);
                    }}
                />
            </div>
            {
                errors ? <span className="errorMessage">{errors}</span> : ""
            }
            {/* {errors && errors.length > 0 ? (
                <div className='has-error'>{errors.join(', ')}</div>
            ) : null} */}
        </div>
    )
}