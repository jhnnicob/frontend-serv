
const routes = [
    {
        path: "/dashboard",
        title: "Dashboard",
    },
    {
        path: "/product",
        title: "Product",
        sub:
            [{ path: `/product/new`, title: "New Product" }]
    }, 
    {
        path: "/unit",
        title: "Unit"
    },
    {
        path: "/order",
        title: "Order"
    }, 
    {
        path: "/user",
        title: "User",
        sub:
            [{ path: `/user/new`, title: "Add User" }]

    },
    {
        path: "/company",
        title: "Your Company",
        sub:
            [{ path: `/company/new`, title: "Add Company" }]
    },
]

export default routes;