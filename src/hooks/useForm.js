import React, {useState, useEffect} from 'react';

export function useForm(initialState, callback, validate) {

    const [values, setValues] = useState({...initialState});
    const [errors, setErrors] = useState({});
    const [isSubmittng, setIsSubmtting] = useState(false);
 
    // const validate = (validations, e) => {
    //     errors && setErrors({
    //         ...errors, [e.target.name]: validations
    //         .map((errorsFor) => errorsFor(e.target.value))
    //         .filter((errorMsg) => errorMsg.length > 0),
    //     });
    // }

    const handleOnChange = (objName) => (e) => {
       
        const name = e.target.name;
        const value = e.target.type == "checkbox" ? 
            e.target.checked : e.target.value;
        if(!objName) {
            setValues({
                ...values, [name]: value
            });
        }else {
            setValues({
                ...values, 
                [objName]: {...values[objName], [name]: value}
            });
        }
    }

    const handleOnSubmit = (e) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmtting(true);
    }

    useEffect(() => {
        if(Object.keys(errors).length === 0 && isSubmittng) {
            callback();
        }
    }, [errors])
    

    return {
        values,
        setValues,
        handleOnChange,
        handleOnSubmit,
        errors,
        setErrors,
        validate
    }
}
