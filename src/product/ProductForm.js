import React from 'react';
import TextField from '../components/text-field';
import CheckBox from '../components/checkbox';
import Button from '../components/button';
import Select from '../components/select';
import useGetData from '../hooks/useGetData';
import { useForm } from '../hooks/useForm';
import {validateProduct} from '../validation';

const defaultValues = {
    product: {
        name: '',
        sku: '',
        description: '',
        company: '',
        basePrice: '',
        baseUnit: '',
        active: false
    }
}

function ProductForm() {

    const {response} = useGetData("company");
    const {
        values, 
        handleOnChange, 
        handleOnSubmit, 
        errors, 
        setValues,
        setErrors} = useForm(defaultValues, submit, validateProduct);

    function submit(){
        console.log("submit")
    }

    return (
        <div className="productForm">
            <TextField 
                type="text"
                label="Product name" 
                name="name"
                value={values.product.name}
                onChange={handleOnChange("product")}
                errors={errors.name}/>
            <Select 
                label="Company" 
                name="company" 
                value={values.product.company}
                options={response.data}
                onChange={handleOnChange("product")}
                errors={errors.company}/>
            <TextField 
                label="Description" 
                name="description"
                value={values.product.description}
                onChange={handleOnChange("product")}
                errors={errors.description}/>
            <TextField 
                label="Sku" 
                name="sku"
                value={values.product.sku}
                onChange={handleOnChange("product")}
                errors={errors.sku}/>
            <TextField 
                label="Base price name" 
                name="basePrice"
                value={values.product.basePrice}
                onChange={handleOnChange("product")}
                errors={errors.basePrice}/>
            <TextField 
                label="Base unit" 
                name="baseUnit"
                value={values.product.baseUnit}
                onChange={handleOnChange("product")}
                errors={errors.baseUnit}/>
            <CheckBox 
                label="Active" 
                name="active" 
                value={values.product.active}
                onChange={handleOnChange("product")}
                errors={errors.active}/>
            <Button color="primary">Save</Button>
        </div>
    )
}

export default ProductForm;
