import React from 'react';
import './Product.css';
import Table from '../components/table';
import { tableConstant } from './tableConstant';
import { DataUnAvailable } from "../components/not-available";
import useGetData from '../hooks/useGetData';
import { Switch,Route } from 'react-router-dom';
import ProductForm from './ProductForm';

export default function Product() {

    const {response, isLoading} = useGetData("product");

    return (
        response.data && response.data.length? (
            <div className="product">
                <Switch>
                    <Route exact path="/product">
                        <h2>Products</h2>
                        <Table cols={tableConstant()} data={response.data} withPagination={true}/>
                    </Route>
                    <Route path="/product/new" component={ProductForm} />
                </Switch>
            </div>
        ) : (
            <DataUnAvailable name="Product"/>
        )

    )
}
