import http from "../http-common";
import authHeader from './auth-header';

const save = (url, obj) => {
    return http.post(url, obj,  {headers: authHeader()});
}

const getAll = (url) => {
    return http.get(`${url}`, {headers: authHeader()});
}

const get = (id, url) => {
    return http.get(`${url}/${id}`);
}


export default {
    getAll,
    get,
    save
}