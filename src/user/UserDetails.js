import React from 'react'

function UserDetails({location}) {

    return (
        <div className="userDetails">
            <h2>{location.state.username}</h2>
            <p>{location.state.email}</p>
        </div>
    )
}

export default UserDetails;
