import React, {useState, useEffect} from 'react';
import './UserForm.css';
import {useForm} from '../hooks/useForm';
import {isRequired, isEmail, validatePerson} from '../validation';
import TextField from '../components/text-field';
import Button from '../components/button';
import Checkbox from '../components/checkbox';
import ApiService from '../services/apiService';
import AlertMessage from '../components/Alert';
import apiService from '../services/apiService';
                
function UserForm({handleOpen}) {

    const initialState = {
        person: {
            firstName: "",
            lastName: "",
        },
        user: {
            username: "",
            email: "",
            password: "",
            confirmPassword: ''
            // role: []
        }
    }


    const {
        values, 
        setValues: setUser,
        handleOnChange,
        handleOnSubmit,
        setErrors, 
        setValues,
        errors, 
        } = useForm(
            initialState, 
            submit, 
            validatePerson
    );

    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [message, setMessage] = useState('');

    function submit() {
        setIsLoading(true);
        apiService.save("/company/person", values)
            .then((response) => {
                if(response.data.success) {
                    setIsLoading(false)
                    setIsSuccess(true);
                    setMessage("Record save!")
                    setValues({...initialState})
                }
            }).catch((e) => {
                setIsLoading(false)
                setValues({...initialState})
                console.log("Error", e);
            })
    }

    return (
        <form className="userForm"
            onSubmit={handleOnSubmit} noValidate>
            {isSuccess ? <AlertMessage message={message}/> : "" }
            <h2>Add User</h2>
            <div className="userForm__row">
                <div className="useForm__col">
                    <TextField 
                        type="text"
                        name="firstName" 
                        label="First name" 
                        value={values.person.firstName}
                        onChange={handleOnChange("person")}
                        errors={errors.firstName}
                        />
                </div>
                <div className="useForm__col">
                    <TextField 
                        type="text"
                        name="lastName" 
                        label="Last name" 
                        value={values.person.lastName}
                        onChange={handleOnChange("person")}
                        errors={errors.lastName}
                        />
                </div>
            </div>
            <TextField 
                type="text"
                name="username" 
                label="User name" 
                value={values.user.username}
                onChange={handleOnChange("user")}
                errors={errors.username}
                />
            <TextField 
                type="text"
                label="Email" 
                name="email" 
                value={values.user.email}
                onChange={handleOnChange("user")}
                errors={errors.email}
                />
            <TextField 
                type="password"
                label="Password" 
                name="password" 
                value={values.user.password}
                onChange={handleOnChange("user")}
                errors={errors.password}
                />
            <TextField 
                type="password"
                name="confirmPassword" 
                label="Confirm Password" 
                value={values.user.confirmPassword}
                onChange={handleOnChange("user")}
                errors={errors.confirmPassword}
                />
            
            <div className="checkboxes">
                <label>Role</label>
                <div className="checkboxes__body">
                    <div className="col">
                        <Checkbox 
                            label="Admin" 
                            name="admin"
                            value="ADMIN"
                            onChange={handleOnChange("user")}
                            />
                        <Checkbox 
                            label="Sales agent" 
                            name="salesAgent" 
                            value="SALES_AGENT"
                            onChange={handleOnChange("user")}
                            />
                    </div>
                    <div className="col">
                        <Checkbox 
                            label="Office Staff"
                            name="officeStaff" 
                            value="OFFICE_STAFF"
                            onChange={handleOnChange("user")}
                            />
                        <Checkbox 
                            label="Sales Manager"
                            name="salesManager"  
                            value="SALES_MANGER"
                            onChange={handleOnChange("user")}
                            />
                    </div>
                </div>
            </div>

            <div className="userForm__buttons">
            <Button color="primary" size="size" disabled={isLoading}>{isLoading ? "Loading..." : "Save"}</Button>
            </div>
        </form>
    )
}

export default UserForm;
