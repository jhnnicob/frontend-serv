import React, {Suspense} from 'react'
import './User.css';
import { tableConstant } from './tableConstant';
import useGetData from '../hooks/useGetData';
import { DataUnAvailable } from '../components/not-available';
import Loader from '../components/loader';
import {Link, Switch, Route} from 'react-router-dom';
import UserDetails from './UserDetails'; 
import UserForm from './UserForm'; 

const UserTable = React.lazy(() => import('./UserTable'));

export default function User() {

    const {response, isLoading} = useGetData('user');
    return (
        isLoading ? (
            <Loader />
        ) : (
            response.data && response.data.length > 0 ? (
                <div className="user">
                    <Switch>
                        <Route exact path="/user">
                            <Suspense fallback={<div>Loading...</div>}>
                                <UserTable cols={tableConstant(Link)} data={response.data}/>
                            </Suspense>
                        </Route>
                        <Route path="/user/new" component={UserForm} />
                        <Route path="/user/:id" component={UserDetails} />
                    </Switch>
                   
                </div>
            ) : (
                <DataUnAvailable name="user"/>
            )
        )
    )
}