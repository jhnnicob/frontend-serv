import React from 'react'

export const tableConstant = (Link) => {
    return [
        {
            title: 'Username',
            render: rowData => {
                return <span>{rowData.username}</span>
            }
        },
        {
            title: 'Email',
            render: rowData => {
                return <span>{rowData.email}</span>
            }
        },
        {
            title: '',
            render: rowData => {
                return <Link to={{
                    pathname:"/user/" + rowData.id,
                    state: rowData
                }}>View</Link>
            }
        },
    ]
}
