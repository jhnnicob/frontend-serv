
export function validateSignup(values) {
  let errors = {};

  if(!values.username) {
    errors.username = "Username is required";
  }else if(values.username.length < 4) {
    errors.username = "Username must at least 6 characters";
  }
  
  if(!values.email) {
    errors.email = "Email is required";
  } else if(!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(values.email)) {
    errors.email = "Email is invalid";
  }

  if(!values.password) {
    errors.password = "Password is required";
  }else if(values.password.length < 6) {
    errors.password = "Password must at least 6 characters";
  }
  return errors;
}

export function validateSignin(values) {
  let errors = {};

  if(!values.username) {
    errors.username = "Username is required";
  }else if(values.username.length < 4) {
    errors.username = "Username must at least 6 characters";
  }


  if(!values.password) {
    errors.password = "Password is required";
  }else if(values.password.length < 6) {
    errors.password = "Password must at least 6 characters";
  }
  return errors;
}

export function validatCompanyData(val) {
  let errors = {};

  //company
  if(!val.company.name) {
    errors.name = "Company name is required";
  }else if(val.company.name.length < 4) {
    errors.name = "Company name must at least 6 characters";
  }

  if(!val.company.address) {
    errors.address = "Address is required";
  }

  // if(!val.company.tin) {
  //   errors.tin = "TIN is required";
  // }else if((val.company.tin.length + 1) <= 12) {
  //   errors.tin = "TIN must 12 digits";
  // }

  //admin
  if(!val.person.firstName) {
    errors.firstName = "First name is required";
  }else if(val.person.firstName.length < 2) {
    errors.firstName = "First name must at least 2 characters";
  }

  if(!val.person.lastName) {
    errors.lastName = "Last name is required";
  }else if(val.person.lastName.length < 2) {
    errors.lastName = "Last name must at least 2 characters";
  }

  //user
  if(!val.user.username) {
    errors.username = "Username is required";
  }else if(val.user.username.length < 3) {
    errors.username = "Username must at least 3 characters";
  }

  if(!val.user.password) {
    errors.password = "Password is required";
  }else if(val.user.password.length < 6) {
    errors.password = "Password must at least 6 characters";
  }

  if(val.user.confirmPassword != val.user.password) {
    errors.confirmPassword = "Confirm your password";
  }

  if(!val.user.confirmPassword) {
    errors.confirmPassword = "Confirm password is required";
  }

  return errors;
}

export function validateUser(val) {

  let errors = {};

  if(!val.username) {
    errors.username = "Username is required";
  }else if(val.username.length < 3) {
    errors.username = "Username must at least 3 characters";
  }

  if(!val.email) {
    errors.email = "Email is required";
  } else if(!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(val.user.email)) {
    errors.email = "Email is invalid";
  }

  if(!val.password) {
    errors.password = "Password is required";
  }else if(val.password.length < 6) {
    errors.password = "Password must at least 6 characters";
  }

  if(val.confirmPassword != val.password) {
    errors.confirmPassword = "Confirm your password";
  }

  if(!val.password) {
    errors.confirmPassword = "Confirm password is required";
  }


  return errors;

}

export function validatePerson(val) {

  let errors = {};

  if(!val.person.firstName) {
    errors.firstName = "First name is required";
  }else if(val.person.firstName.length < 2) {
    errors.firstName = "First name must at least 2 characters";
  }

  if(!val.person.lastName) {
    errors.lastName = "Last name is required";
  }else if(val.person.lastName.length < 2) {
    errors.lastName = "Last name must at least 2 characters";
  }


  if(!val.user.username) {
    errors.username = "Username is required";
  }else if(val.user.username.length < 3) {
    errors.username = "Username must at least 3 characters";
  }

  if(!val.user.email) {
    errors.email = "Email is required";
  } else if(!/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(val.user.email)) {
    errors.email = "Email is invalid";
  }

  if(!val.user.password) {
    errors.password = "Password is required";
  }else if(val.user.password.length < 6) {
    errors.password = "Password must at least 6 characters";
  }

  if(val.user.confirmPassword != val.user.password) {
    errors.confirmPassword = "Confirm your password";
  }

  if(!val.user.confirmPassword) {
    errors.confirmPassword = "Confirm password is required";
  }

  return errors;

}

export function validateProduct(val) {
  
}


// export function isRequired(val) {
//   return val.length > 0 ? '' : 'cannot be blank';
// }

// export function isEmail(val) {
//   const ai = val.indexOf('@');
//   const gdi = val
//     .split('')
//     .reduce((acc, char, i) => (char === '.' ? i : acc), 0);
//   return ai > -1 && gdi > ai ? '' : 'must be an email';
// }

// export function charLimit(val) {
//   return val.length < 3 ? '' : 'must at least 3 characters';
// }